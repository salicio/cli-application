<?php

namespace App\Test\Command;

use App\Command\HelloCommand;
use App\Console\Application;
use App\Service\HelloService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\CommandTester;

class HelloCommandTest extends TestCase
{
    private $commandTester;

    protected function setUp(): void
    {
        $application = new Application([
            new HelloCommand(new HelloService())
        ]);

        $command = $application->find('app:hello');
        $this->commandTester = new CommandTester($command);
    }

    public function testExecuteSuccess()
    {
        $this->assertSame(0, $this->commandTester->execute([]));
    }

    public function testOutput()
    {
        $exitCode = $this->commandTester->execute([]);

        $this->assertEquals("Hello\n", $this->commandTester->getDisplay());
    }
}
