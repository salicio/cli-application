<?php

namespace App\Test\Service;

use App\Service\HelloService;
use PHPUnit\Framework\TestCase;

class HelloServiceTest extends TestCase
{
    private $service;

    protected function setUp(): void
    {
        $this->service = new HelloService();
    }

    public function testGreet()
    {
        $this->assertEquals('Hello', $this->service->greet());
    }
}
