<?php

namespace App\Test\Console;

use App\Console\Application;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\ApplicationTester;

class ApplicationTest extends TestCase
{
    private $app;

    protected function setUp(): void
    {
        $application = new Application();
        $application->setAutoExit(false);
        $this->app = new ApplicationTester($application);
    }

    public function testNoError()
    {
        $input = [];

        $exitCode = $this->app->run($input);

        $this->assertSame(0, $exitCode, $this->app->getDisplay());
    }
}
