<?php

declare(strict_types=1);

namespace App\Console;

use Symfony\Component\Console\Application as BaseApplication;
use Symfony\Component\Console\Command\Command;

class Application extends BaseApplication
{
    const NAME = 'Application';
    const VERSION = '1.0';

    /**
      * Constructor.
      *
      * @param iterable<Command> $commands
      */
    public function __construct(iterable $commands = [])
    {
        parent::__construct(self::NAME, self::VERSION);

        foreach ($commands as $command) {
            $this->add($command);
        }
    }
}
