<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\HelloService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HelloCommand extends Command
{
    protected static $defaultName = 'app:hello';

    /**
     * @var HelloService
     */
    private $helloService;

    public function __construct(HelloService $helloService)
    {
        parent::__construct();

        $this->helloService = $helloService;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Say hello.')
            ->setHelp('This command allows you to say hello.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln($this->helloService->greet());

        return Command::SUCCESS;
    }
}
