<?php

declare(strict_types=1);

namespace App\Service;

class HelloService
{
    public function greet(): string
    {
        return "Hello";
    }
}
